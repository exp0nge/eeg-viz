import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
from scipy.stats import zscore

# coding: utf-8

# In[1]:

from scipy.io import loadmat
from scipy import signal
from eegtools.io import load_edf

# In[2]:

m = load_edf('suj5_d2_nap-export_0001.edf')

# In[3]:

#matlab load
#matrix = m['s5d2nap']

#edf load
matrix = m[0]

#lowpass
b1, a1 = signal.butter(2, 0.1, 'low', analog=True)
w1, h1 = signal.freqs(b1, a1)

#highpass
b2, a2 = signal.butter(2, 0.5, 'high', analog=True)
w2, h2 = signal.freqs(b2, a2)

low = 0.3
high = 30
fs = 1000

lowcut = low/(0.5*fs)
highcut = high/(0.5*fs)


#bandpass
b3, a3 = signal.butter(2, [lowcut,highcut], 'band')
w3, h3 = signal.freqs(b3, a3)

#create matrix
new_matrix = []

for index, row in (enumerate(matrix)):
	if(index!=63 and index!=62 and index!=61):
		print index
		new_matrix.append(row[0:600000])
# print len(new_matrix[0])

#detrend and make the mean 0 for the data
#eeg = signal.detrend(np.array(new_matrix),type='constant')


#output the raw data


#normal print
#]mat = []
#for row in range(len(new_matrix)):
#	mat.append(eeg[row])

#s = pd.DataFrame(mat).transpose()

#lowpass print
#mat1 = []
#for row in range(len(new_matrix)):
#	mat1.append(signal.lfilter(b1, a1, eeg[row]))

#s1 = pd.DataFrame(mat1).transpose()

#highpass print
#mat2 = []
#for row in range(len(new_matrix)):
#	mat2.append(signal.lfilter(b2, a2, eeg[row]))
#s2 = pd.DataFrame(mat2).transpose()

#highpass print
#mat2 = []
#for row in range(len(new_matrix)):
#	mat2.append(signal.lfilter(b2, a2, eeg[row]))
#s2 = pd.DataFrame(mat2).transpose()

#bandpass
mat3 = []
for row in range(len(eeg)):
	mat3.append(signal.lfilter(b3, a3, eeg[row]))

#clipped
#stemp = pd.DataFrame(mat3).clip(-300,300)
stemp = pd.DataFrame(mat3)

s3 = pd.DataFrame((stemp))
#s3 = pd.DataFrame(eeg)

band = np.array(s3)

band.dump('ICA_band1.dumps')
#print eeg.shape
#eeg.dump('ICA_data_raw.dumps')

# In[ ]:
#s.plot(legend=False)
#s1.plot(legend=False)
#s2.plot(legend=False)
print "\n Huh?"
s3.plot(legend=False)
print "Did that work?"
# cumsum() adds the value of each channel and displays the sum
# s = s.cumsum()
# s.plot()


# fig = plt.figure()
# fig.savefig('matrix.svg')


#plt.figure(2);

# plt.plot(eeg[:8,:30000].T + 8000*np.arange(7,-1,-1));

# plt.plot(np.zeros((30000,8)) + 8000*np.arange(7,-1,-1),'--',color='gray');

#plt.yticks([]);

# plt.legend(first['channels']);

#plt.axis('tight');


#plt.show()
